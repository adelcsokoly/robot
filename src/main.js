let words = [];
let wordFall = 0;
let letters = ['I\'m not a robot'];
let focused = true; //?
let animation = [];
let score = 28800;
let audio = [
    new Audio('https://adelcsokoly.gitlab.io/robot/resources/sounds/hoe_1.wav'),
    new Audio('https://adelcsokoly.gitlab.io/robot/resources/sounds/hoe_2.wav'),
    new Audio('https://adelcsokoly.gitlab.io/robot/resources/sounds/hoe_3.wav'),
    new Audio('https://adelcsokoly.gitlab.io/robot/resources/sounds/hoe_4.wav'),
    new Audio('https://adelcsokoly.gitlab.io/robot/resources/sounds/hoe_5.wav'),
    new Audio('https://adelcsokoly.gitlab.io/robot/resources/sounds/hoe_6.wav'),
    new Audio('https://adelcsokoly.gitlab.io/robot/resources/sounds/hoe_7.wav'),
    new Audio('https://adelcsokoly.gitlab.io/robot/resources/sounds/hoe_8.wav')
]

function setup() {
  createCanvas(windowWidth, windowHeight);
  textAlign(LEFT, CENTER);
  rectMode(CENTER);  //?
}

function draw() {
  background(255);

  textAlign(RIGHT, CENTER);
  textSize(20);
  textStyle(NORMAL);
  noStroke();
  fill('black');
  text(score, windowWidth - 40, 40);

  for (var i = 0; i <= animation.length -1; i++) {
    animation[i].display();
    animation[i].fade()
    if (animation[i].isEnded()) {
      animation.splice(i, 1); //eltüntet
    }
  }
  for (var i = words.length - 1; i >= 0; i--) {
    words[i].display();
    if (focused === true) {
      words[i].move();
    }

    if (words[i].isOffScreen()) {
      words.splice(i, 1);
    }
  }

  if (focused === true) {
    wordFall += deltaTime * 0.0025 //sűrűség
    if (wordFall >= 1) {
      wordFall = 0
      makeWord()
    }
  }
}

function mouseClicked() {
  animateTarget();
}

function mouseReleased() {
  animateTarget();
}

function animateTarget() {
  for (var i = words.length - 1; i >= 0; i--) {
    if (words[i].isClicked()) {
      console.log(words[i])
      score--;
      makeAnimation(words[i].x, words[i].y)
      makeSound();
      words.splice(i, 1); //eltüntet
    }
  }
}

function makeSound() {
  let currentSound = Math.floor(Math.random() * audio.length);
  audio[currentSound].play();
}

function windowResized() {
  createCanvas(windowWidth, windowHeight);
}

function makeWord() { //hozzáad egy üres listát
  words.push(new Word());
}

function makeAnimation(x, y) {
  animation.push(new FadeOut(x, y));
}

class FadeOut {
  constructor(x, y) {
    this.x = x;
    this.y = y;
    this.size = 16;
    this.l = 'I\'m not a robot';
    this.w = textWidth(this.l);
    this.color = 0;
    this.speed = 1;
  }

  isEnded() {
    if (this.color > 255) {
      return true
    }
  }

  fade() {
    this.color += this.speed;

  }
  display() {
    fill(255) //keret
    stroke(this.color)
    strokeWeight(1);
    rect(this.x - this.size * 0.5 + textWidth(letters) * 0.5, this.y - 1, this.size + textWidth(letters) * 1.15, this.size * 1.5);

    fill(this.color); //szöveg
    textSize(this.size);
    noStroke();
    textAlign(LEFT, CENTER);
    text(this.l, this.x, this.y);

    noFill() //checkbox
    stroke(this.color)
    strokeWeight(1);
    rect(this.x - this.size * 0.75, this.y - 1, this.size, this.size, this.size * 0.25);

    push()
    translate(this.x - this.size * 0.55, this.y - 3) //pipa helye a boxban
    fill(this.color)
    noStroke() //pipa
    drawCheckmark(this.size * 1.2)
    pop() //
  }

}

function drawCheckmark(s) { //pipa
  beginShape();
  vertex(s/2, -s/2)
  vertex(-s/4, s/2)
  vertex(-s/2, 0)
  vertex(-s/4, s/8)
  endShape(CLOSE);
}

class Word {
  constructor() {  //alap tul
    this.size = 16;
    this.l = 'I\'m not a robot';
    this.w = textWidth(this.l); //szélesség
    this.x = random(32, width - 32 - this.w); //hol az x tengelyen
    this.y = 0; //hol az yon
    this.speed = 1;
    this.color = color(0);
  }

  move() {
    this.y += this.speed;
  }

  display() { //hogyan jelenítse meg a dolgokat
    fill(255) //keret
    stroke(this.color)
    strokeWeight(1);
    rect(this.x - this.size * 0.5 + textWidth(letters) * 0.5, this.y - 1, this.size + textWidth(letters) * 1.15, this.size * 1.5);

    fill(this.color); //szöveg
    textSize(this.size);
    textAlign(LEFT, CENTER);
    noStroke();
    text(this.l, this.x, this.y);

    noFill() //checkbox
    stroke(this.color)
    strokeWeight(1);
    rect(this.x - this.size * 0.75, this.y - 1, this.size, this.size, this.size * 0.25);
  }

  isOffScreen() { //csekkolja kiért e a képernyőaljára
    if (this.y > height) {
      return true
    }
  }

  isClicked() { //rá van e kattintva
    if (this.x - this.size * 0.5 + textWidth(letters) * 0.5 - 0.5 * (this.size + textWidth(letters) * 1.15) <= mouseX && this.x - this.size * 0.5 + textWidth(letters) * 0.5 + 0.5 * (this.size + textWidth(letters) * 1.15) >= mouseX && this.y - 1 - 0.5 * (this.size * 1.5) <= mouseY && this.y - 1 + 0.5 * (this.size * 1.5) >= mouseY) {
      return true;
    }
  }
}

window.addEventListener('focus', function(event) {
  focused = true //csak akkor jöjjenek a szavak, ha ezt az ablakot nézzük
}, false)

window.addEventListener('blur', function(event) {
  focused = false
}, false)
